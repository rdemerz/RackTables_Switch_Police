#!/bin/bash -x

#My Permanent temporary solution.
#TODO: Use dictionary/hash table instead of writing out same logic for every model
#TODO: Beautify this code.... Too much repeating code
function process() {
	if [ $noMore == "yes" ]; then
		return
	fi
	local _ip=$1
        local _port=$2
        local _vlan=$3
	local _swType=$4
        if [[ $_swType == "G8052" ]]
        then
		pvid=$(snmpget -v 2c -c <COMMUNITY> -Ln $ipaddr .1.3.6.1.4.1.26543.2.7.7.1.1.2.3.1.6.$_port | awk '{print $4}')
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.26543.2.7.7.1.1.2.3.1.6.$_port i $_vlan
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.26543.2.7.7.1.1.1.2.0 i 2
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.26543.2.7.7.1.1.1.4.0 i 2
        elif [[ $_swType == "G7052" ]]
        then
		pvid=$(snmpget -v 2c -c <COMMUNITY> -Ln $ipaddr .1.3.6.1.4.1.20301.2.7.18.1.1.2.3.1.6.$_port | awk '{print $4}')
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.20301.2.7.18.1.1.2.3.1.6.$_port i $_vlan
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.20301.2.7.18.1.1.1.2.0 i 2
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.20301.2.7.18.1.1.1.4.0 i 2
        elif [[ $_swType == "G7028" ]]
        then
		pvid=$(snmpget -v 2c -c <COMMUNITY> -Ln $ipaddr .1.3.6.1.4.1.20301.2.7.17.1.1.2.3.1.6.$_port | awk '{print $4}')
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.20301.2.7.17.1.1.2.3.1.6.$_port i $_vlan
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.20301.2.7.17.1.1.1.2.0 i 2
                snmpset -c '<SNMP PASSWORD>' -v 2c -Ln $_ip .1.3.6.1.4.1.20301.2.7.17.1.1.1.4.0 i 2
        else
                #echo "wow, we are lost" >> /tmp/test_debug
                exit 1
        fi
}

function alreadyDisabled() {
	alreadyDis=$(mysql -h'<host>' -N -s -P'3306' -u'root' -p'<PASSWORD>' -D'unregistered_macs' -e "select * from rerouted where switch=\"$1\" and interface=$2;")
	if [[ -n $alreadyDis ]]; then
		echo "port $2 on switch $1 already disabled" >> /root/scripts/reportedTraps.txt
		echo "already disabled"
		exit 0
	fi
}



#############################################
# START - BORING      
#############################################

echo "$(date) $1 reported port $2" >> /root/scripts/reportedTraps.txt

routers=(<LIST OF IBM SWITCHES TO MONITOR>)
if [ -z $(echo ${routers[*]} | grep  "$1 ") ]
then
	echo "Not in list of monitored switches"
	exit 0
fi
ipaddr=$1
intr=$2         # REPORTED INTERFACE
intrQuery=$(expr $2 - 4)
macTemp=$(mktemp)
trap "rm -f $macTemp" EXIT
type=$(snmpget -Ln -On -v 2c -c <COMMUNITY> $ipaddr  sysDescr.0 | awk '{print $NF}' | cut -f1 -d"\"")    #get switch model

#############################################  
# GET OCTS
#############################################

if [ $type == "G8052" ]; then
	intrQuery=$(expr $intr - 128)
	alreadyDisabled $ipaddr $intrQuery
	interface=($(snmpwalk -Ln  -v 2c -c <COMMUNITY> $ipaddr -On iso.3.6.1.2.1.17.4.3.1.2 | egrep  ": $intrQuery$" | cut -f1 -d" " | cut -f13- -d.))
elif [ $type == "G7052" ]; then
	alreadyDisabled $ipaddr $intrQuery
	interface=($(snmpwalk -Ln -v 2c -c <COMMUNITY> $ipaddr -Cc .1.3.6.1.4.1.20301.2.7.18.2.3.1.2.1.3 -On | egrep ": $intrQuery$" | cut -f1 -d" " | cut -f18- -d.))
elif [ $type == "G7028" ]; then
	alreadyDisabled $ipaddr $intrQuery
	interface=($(snmpwalk -Ln -v 2c -c <COMMUNITY> $ipaddr -Cc .1.3.6.1.4.1.20301.2.7.17.2.3.1.2.1.3 -On | egrep ": $intrQuery$" | cut -f1 -d" " | cut -f18- -d.))
fi
if [ ${#interface[@]} -gt 2 ]; then
	echo  "$(date) Multiple Macs detected on pPort $intrQuery on $ipaddr ($type).." >> /root/scripts/disabledSwitches.txt
	exit 1
fi
octcounter=0
addon=""
for oct in "${interface[@]}"; do
	if [ -z "$oct" ]; then
		continue        
	fi
	counter=0
	mac=""
	until [[ -n $mac || $counter -gt 10 ]]; do
		let counter+=1
		if [ $type == "G8052" ]; then
			snmpwalk -Ln -On -m ALL -v2c -c <COMMUNITY> $ipaddr .1.3.6.1.2.1.17.4.3.1.1 > $(echo $macTemp)
			mac=$(grep "$oct " $macTemp | cut -f4-9 -d" " |  sed  's/\ /:/g')
		elif [ $type == "G7052" ]; then
			snmpwalk -Ln -v 2c -c <COMMUNITY> $ipaddr -Cc -On .1.3.6.1.4.1.20301.2.7.18.2.3.1.2.1.1  > $(echo $macTemp)
			mac=$(grep "$oct " $macTemp | awk '{print $4}' | sed 's/\b\(\w\)\b/0\1/g')
		elif [ $type == "G7028" ]; then
			snmpwalk -Ln -v 2c -c <COMMUNITY>  $ipaddr -On .1.3.6.1.4.1.20301.2.7.17.2.3.1.2.1.1 > $(echo $macTemp)
                        mac=$(grep "$oct " $macTemp | awk '{print $4}' | sed 's/\b\(\w\)\b/0\1/g')
			
		fi
		if [ -z "$mac" ]; then
			sleep 10
		fi

	done
	macFound=$(mysql -h'<host>' -N -s -P'3306' -u'root' -p'<PASSWORD>' -D'racktables_db' -e "select l2address from Port where l2address='`echo ${mac//:/}`';")
	if [[ -z "$macFound" ]]; then
		addon+="$mac "
	fi
done
if [ -z "$addon" ]; then
	exit 0;
else
	location=$(mysql -h'<host>' -Nt -s -P'3306' -u'root' -p'<PASSWORD>' -D'racktables_db' -e "select distinct Rack.name, row_name, location_name, AttributeValue.string_value from Rack Right Join RackSpace on Rack.id=RackSpace.rack_id Right Join Object on RackSpace.object_id=Object.id RIGHT JOIN AttributeValue on AttributeValue.object_id=Rack.location_id where Object.name='`echo $ipaddr`' and AttributeValue.attr_id=14;")

	labOwner=$(echo $location | awk -F"|" '{print $5}')

	loc=$(echo $location | awk -F"|" '{print $2">"$3">"$4}')
	
	 for m in $addon; do
                alreadyPresent=$(mysql -h'<host>' -N -s -P'3306' -u'root' -p'<PASSWORD>' -D'unregistered_macs' -e "select * from rerouted where mac=\"$m\";")
                if [[ -n $alreadyPresent ]]; then
			process $(echo $alreadyPresent | awk '{print $3" "$4" "$5}') $type
		else
			process $ipaddr $intrQuery 56 $type 
                fi
		noMore="yes"
                mysql -h'<host>' -N -s -P'3306' -u'root' -p'<PASSWORD>' -D'unregistered_macs' -e "insert into rerouted(mac, switch, interface, vlan, location) values (\"${m//:/}\", \"$ipaddr\", $intrQuery, $pvid, \"$loc\") on DUPLICATE KEY UPDATE switch=\"$ipaddr\", interface=$intrQuery, location=\"$loc\";"
        done


#####################################
#	MESSAGING
#######################################



	message="Port $intrQuery on $ipaddr is disabled because connected device's mac address(es) is(are) not in racktables. Please update Racktables.\n\n$addon \n\nSwitch Name: $ipaddr \t Switch Type: $type \nAsset Location: $loc \nLab Owner: $labOwner"

	subject="Racktables violation (port $intrQuery on $ipaddr)"

        echo  "$(date +%k%M) Port $intrQuery on $ipaddr with mac $mac" >> /root/scripts/notifications.txt
        echo -e "$message" | mailx -s "$subject" -r "Switch-Patrol" -c $labOwner -c <email-address>
	echo  "$(date) Disabled Port $intrQuery on $ipaddr ($type - try $counter ) in $loc because ${addon/ / and } is/are not in Racktables.." >> /root/scripts/disabledSwitches.txt

fi
exit 0