# RackTables_Switch_Police

**Racktables Switch Police**

*To stop devs and engineers from wasting my time trying to gather basic info to troubleshoot their server issue.*

This will force users to enter all information about their server into Racktables (or so they believe, I just check for Mac address) to be able to access the network.
If they fail to enter the required info, the port's pvid will be changed and they will not be able work with their server.

**REQUIRES:**
* Racktables
* MYSQL/MariaDB - triggers
* SNMP - Ability to receive traps

